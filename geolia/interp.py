#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of geolia
# License: GPLv3
# See the documentation at  gitlab.com/benvial/geolia


"""Interpolation tools"""

import numpy as np
from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator


def interpolator(meshobject, cell, kind="nearest", **kwargs):
    """
    Build an interpolator for a given cell in a mesh object.

    Parameters
    ----------
    meshobject : meshio.Mesh
        The mesh object.
    cell : str
        The name of the cell data to interpolate.
    kind : str, optional
        The interpolation kind. Choose between 'nearest' or 'linear'.
    **kwargs : dict, optional
        Additional keyword arguments are passed to the interpolator constructor.

    Returns
    -------
    Interp : scipy.interpolate.NDInterpolatorBase
        The interpolator object.
    """
    mesh = meshobject[cell]
    if kind not in ["nearest", "linear"]:
        raise ValueError(
            f"Wrong interpolation kind: {kind}, Choose betwwen 'nearest' or 'linear'"
        )
    points = np.mean(mesh.points[mesh.cells[0].data], axis=1)
    values = mesh.cell_data[cell][0]
    Interp = NearestNDInterpolator if kind == "nearest" else LinearNDInterpolator
    return Interp(points, values, **kwargs)


def interpolate(grid, meshobject, cell, kind="nearest", **kwargs):
    """
    Interpolate data from a mesh object at points in a grid.

    Parameters
    ----------
    grid : tuple of arrays
        The grid points to interpolate at.
    meshobject : meshio.Mesh
        The mesh object.
    cell : str
        The name of the cell data to interpolate.
    kind : str, optional
        The interpolation kind. Choose between 'nearest' or 'linear'.
    **kwargs : dict, optional
        Additional keyword arguments are passed to the interpolator constructor.

    Returns
    -------
    array
        The interpolated data at the grid points.
    """
    interp = interpolator(meshobject, cell, kind=kind, **kwargs)
    return interp(*grid)
