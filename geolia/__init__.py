#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of geolia
# License: GPLv3
# See the documentation at  gitlab.com/benvial/geolia

# flake8: noqa

"""Geometry and mesh tools

geolia is a Python package for defining and meshing geometries using Gmsh.
"""

from .__about__ import __author__, __description__, __version__
from .geometry import *
from .interp import *
from .mesh import *
