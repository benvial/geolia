#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of geolia
# License: GPLv3
# See the documentation at  gitlab.com/benvial/geolia


"""Meshing utilities"""


__all__ = ["read_mesh", "mesh_to_triangulation", "plot_mesh"]

import tempfile

import matplotlib.tri as tri
import meshio
import numpy as np


def read_mesh(
    mesh_file, data_dir=None, data_dir_xdmf=None, dim=3, subdomains=None, quad=False
):
    """
    Read a mesh file and return a dictionary of meshio.Mesh objects.

    Parameters
    ----------
    mesh_file : str
        The path to the mesh file.
    data_dir : str
        The directory where the mesh data will be stored.
    data_dir_xdmf : str
        The directory where the xdmf files will be stored.
    dim : int
        The dimension of the mesh.
    subdomains : list of str or str
        The subdomains to be read.
    quad : bool
        If True, the mesh will be read as a quadrangular mesh.

    Returns
    -------
    dict
        A dictionary of meshio.Mesh objects.

    Notes
    -----
    The meshio_mesh is a meshio object and can be used directly.

    """
    data_dir_xdmf = data_dir_xdmf or tempfile.mkdtemp()
    # Read the mesh file
    meshio_mesh = meshio.read(mesh_file, file_format="gmsh")

    # Determine the base cell type
    if dim == 3:
        base_cell_type = "hexahedron" if quad else "tetra"
    elif dim == 2:
        base_cell_type = "quad" if quad else "triangle"
    else:
        base_cell_type = "line"

    # Get the points and physicals
    points = meshio_mesh.points if dim == 3 else meshio_mesh.points[:, :2]
    physicals = meshio_mesh.cell_data_dict["gmsh:physical"]

    # Split the data into different cell types
    cell_types, data_gmsh = zip(*physicals.items())
    data_gmsh = list(data_gmsh)
    cells = {ct: [] for ct in cell_types}

    # Loop over the cell types and create a list of cells for each
    for cell_type in cell_types:
        cells[cell_type] = meshio_mesh.cells_dict[cell_type]

    # Determine the index of the base cell type
    icell = np.where(np.array(cell_types) == base_cell_type)[0][0]

    # If subdomains are provided, select the cells that belong to the
    # subdomains
    if subdomains is not None:
        doms = subdomains if hasattr(subdomains, "__len__") else [subdomains]
        mask = np.isin(data_gmsh[icell], doms)
        data_gmsh_ = data_gmsh[icell][mask]
        data_gmsh[icell] = data_gmsh_
        cells[base_cell_type] = cells[base_cell_type][mask]

    # Create a dictionary of meshio.Mesh objects
    mesh_data = {}

    # Loop over the cell types and create a meshio.Mesh object for each
    for cell_type, data in zip(cell_types, data_gmsh):
        meshio_data = meshio.Mesh(
            points=points,
            cells={cell_type: cells[cell_type]},
            cell_data={cell_type: [data]},
        )
        # Write the mesh to an xdmf file
        meshio.xdmf.write(f"{data_dir_xdmf}/{cell_type}.xdmf", meshio_data)
        mesh_data[cell_type] = meshio_data
    return mesh_data


def mesh_to_triangulation(mesh):
    """Convert a meshio Mesh object to a matplotlib Triangulation object.

    Parameters
    ----------
    mesh : meshio.Mesh
        The meshio Mesh object.

    Returns
    -------
    triangulation : matplotlib.tri.Triangulation
        The matplotlib Triangulation object.
    """

    points = mesh.points
    triangles = mesh.cells[0].data
    return tri.Triangulation(points[:, 0], points[:, 1], triangles)


def plot_mesh(ax, mesh, **kwargs):
    """
    Plot a meshio Mesh object as a matplotlib triangulation.

    Parameters
    ----------
    ax : matplotlib Axes
        The axes to plot on.
    mesh : meshio.Mesh
        The meshio Mesh object.
    **kwargs : dict
        Additional keyword arguments are passed to `ax.triplot`.

    Returns
    -------
    line : matplotlib.lines.Line2D
        The matplotlib Line2D object.

    """
    color = kwargs.pop("color", "#808080")
    line = ax.triplot(mesh_to_triangulation(mesh), color=color, **kwargs)
    ax.set_aspect("equal")
    return line
