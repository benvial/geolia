#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of geolia
# License: GPLv3
# See the documentation at  gitlab.com/benvial/geolia


"""
Geometry definition using Gmsh api.
For more information see Gmsh's `documentation <https://gmsh.info/doc/texinfo/gmsh.html>`_
"""

__all__ = ["Geometry", "plot_geometry"]

import numbers
import os
import re
import tempfile
from functools import wraps

import gmsh
import matplotlib.pyplot as plt
import numpy as np

from .mesh import mesh_to_triangulation, read_mesh

geo = gmsh.model.geo
occ = gmsh.model.occ
model = gmsh.model
setnum = gmsh.option.setNumber
gmsh_options = gmsh.option


def _set_opt_gmsh(name, value):
    """
    Set a Gmsh option.

    Parameters
    ----------
    name : str
        The name of the option.
    value : str or number or set or tuple or list
        The value to set. If a boolean, it is converted to an integer.

    Returns
    -------
    None

    Raises
    ------
    ValueError
        If the value is not a string, a number, a set, a tuple or a list of length 3.

    Notes
    -----
    See `Gmsh's documentation <https://gmsh.info/doc/texinfo/gmsh.html#Option-command>`_
    for the list of available options.

    """
    if isinstance(value, str):
        return gmsh_options.set_string(name, value)
    elif isinstance(value, set) or isinstance(value, tuple) or isinstance(value, list):
        return gmsh_options.set_color(name, *value)
    elif isinstance(value, (numbers.Number, bool)):
        if isinstance(value, bool):
            value = int(value)
        return gmsh_options.set_number(name, value)
    else:
        raise ValueError(
            "value must be string or number or any of set/tuple/list of length 3 (for setting colors)"
        )


def _get_opt_gmsh(name):
    """
    Get a Gmsh option.

    Parameters
    ----------
    name : str
        The name of the option.

    Returns
    -------
    str or number or set or tuple or list of length 3
        The value of the option.

    Notes
    -----
    See `Gmsh's documentation <https://gmsh.info/doc/texinfo/gmsh.html#Option-command>`_
    for the list of available options.

    """
    try:
        return gmsh_options.get_number(name)
    except Exception:
        try:
            return gmsh_options.get_string(name)
        except Exception:
            try:
                return gmsh_options.get_color(name)
            except Exception:
                return None


setattr(gmsh_options, "set", _set_opt_gmsh)
setattr(gmsh_options, "get", _get_opt_gmsh)


def _add_method(cls, func, name):
    """
    Add a method to the class `cls` with name `name`.

    The method will be a wrapper around `func` with an additional argument
    `sync` which defaults to `True`.

    If `sync` is `True` and the method is called, it will call
    `occ.synchronize` after the method has been called.

    The original method is returned.

    """

    @wraps(func)
    def wrapper(*args, sync=True, **kwargs):
        if not gmsh.is_initialized():
            raise RuntimeError("Geometry is not initialized")
        out = func(*args, **kwargs)
        if sync:
            occ.synchronize()
        return out

    setattr(cls, name, wrapper)
    return func


def _dimtag(tag, dim=2):
    """
    Convert an integer or list of integer to gmsh DimTag notation.

    Parameters
    ----------
    tag : int or list of int
        Label or list of labels.
    dim : int
        Dimension.

    Returns
    -------
    int or list of int
        A tuple (dim, tag) or list of such tuples (gmsh DimTag notation).

    """
    if not isinstance(tag, list):
        tag = [tag]
    return [(dim, t) for t in tag]


def _get_bnd(idf, dim):
    """
    Get the boundary entities of a given entity.

    Parameters
    ----------
    idf : int or list of int
        Label or list of labels.
    dim : int
        Dimension.

    Returns
    -------
    list of int
        A list of entity tags of the boundary entities.

    """

    out = gmsh.model.getBoundary(_dimtag(idf, dim=dim), False, False, False)
    return [b[1] for b in out]


def _convert_name(name):
    """
    Convert a name to a valid variable name.

    Parameters
    ----------
    name : str
        The name to convert.

    Returns
    -------
    str
        The converted name.

    Notes
    -----
    This function is used to convert names of entities so that they can be used
    as variable names in Python.

    Examples
    --------
    >>> _convert_name('MyName')
    'my_name'

    """
    return re.sub(r"(?<!^)(?=[A-Z])", "_", name).lower()


def geowrapp(method, self, dim=None):
    """
    A decorator that wraps a Gmsh method to return a GeoBase object.

    Parameters
    ----------
    method : function
        The Gmsh method to wrap.
    self : object
        The instance of the class calling the method.
    dim : int, optional
        The dimension of the entity to create.

    Returns
    -------
    function
        The decorated function.

    Notes
    -----
    This function is used internally to wrap Gmsh methods and return
    GeoBase objects.

    """

    def wrapper(*args, **kwargs):
        """
        A wrapper for a Gmsh method to return a GeoBase object.

        Parameters
        ----------
        *args : tuple
            Arguments to be passed to the Gmsh method.
        **kwargs : dict
            Keyword arguments to be passed to the Gmsh method.

        Returns
        -------
        GeoBase
            A GeoBase object created from the return value of the Gmsh method.

        """
        tag = method(*args, **kwargs)
        # self = args[0]
        dimtag = self.dimtag(tag) if dim is None else _dimtag(tag, dim)
        return GeoBase(dimtag)

    return wrapper


def _add_gmsh_methods(self):
    for object_name in dir(occ):
        if (
            not object_name.startswith("__")
            and object_name != "mesh"
            and object_name not in dir(self)
        ):
            bound_method = getattr(occ, object_name)
            name = _convert_name(bound_method.__name__)
            if name.startswith("add_"):
                dim = None
                if name == "add_point":
                    dim = 0
                if name in ["add_line"]:
                    dim = 1
                bound_method = geowrapp(bound_method, self, dim)
            _add_method(self, bound_method, name)


def is_iter(element):
    """
    Check if an element is iterable.

    Parameters
    ----------
    element : object
        Element to check.

    Returns
    -------
    bool
        True if element is iterable, False otherwise.

    """

    try:
        iter(element)
    except TypeError:
        return False
    return True


def _check_length(x):
    """
    Check the length of x and return either the single element or the
    list itself.

    Parameters
    ----------
    x : list
        The list to check.

    Returns
    -------
    list or object
        The single element of x if it has length 1, otherwise x itself.

    """
    return x[0] if len(x) == 1 else x


def __boolean_operation__(self, other, function):
    """
    A private method to perform a boolean operation between self and other.

    Parameters
    ----------
    other : GeoBase or list of GeoBase
        The other geometry to perform the boolean operation with.
    function : callable
        The function to use for the boolean operation. It should take a single
        list argument and return a tuple of two elements: the first element is a
        list of generated entities, and the second element is a list of parent
        child relationships.

    Returns
    -------
    list of GeoBase
        The result of the boolean operation.

    Notes
    -----
    The function is called with a single argument, which is a list of the
    input geometry. The function should return a tuple of two elements: the
    first element is a list of generated entities, and the second element is a
    list of parent child relationships.

    """
    otag = [o.dimtag for o in other] if is_iter(other) else [other.dimtag]
    out, outmap = function([self.dimtag], otag)
    occ.synchronize()
    geo_out = [GeoBase(o) for o in out]
    # # out contains all the generated entities of the same dimension as the input
    # # entities:
    # print("fragment produced volumes:")
    # for e in out:
    #     print(e)
    # # outmap contains the parent-child relationships for all the input entities:
    # print("before/after fragment relations:")
    # for e in zip([self.dimtag, other.dimtag], outmap):
    #     print("parent " + str(e[0]) + " -> child " + str(e[1]))

    return _check_length(geo_out)


class GeoBase:
    """
    A Geometry object.

    Parameters
    ----------
    dimtag : tuple of int
        The dimension and tag of the entity.
    name : str, optional
        The name of the entity.
    physical_tag : int, optional
        The physical tag of the entity.

    Attributes
    ----------
    dimtag : tuple of int
        The dimension and tag of the entity.
    name : str
        The name of the entity.
    physical_tag : int
        The physical tag of the entity.
    """

    def __init__(self, dimtag, name=None, physical_tag=None):
        self.dimtag = _check_length(dimtag)
        self.name = name or f"geo_{self.__hash__()}"
        self.physical_tag = physical_tag

    @property
    def dim(self):
        """
        The dimension of the entity.

        Returns
        -------
        int
            The dimension of the entity.
        """
        return self.dimtag[0]

    @property
    def tag(self):
        """
        The tag of the entity.

        Returns
        -------
        int
            The tag of the entity.
        """
        return self.dimtag[1]

    @property
    def tags(self):
        """
        Get the tags of the entity.

        Returns
        -------
        dict
            A dictionary with keys "geometrical" and "physical" containing the
            respective tags.
        """
        return dict(geometrical=self.tag, physical=self.physical_tag)

    def __repr__(self):
        """
        Return a string representation of the GeoBase object.

        Returns
        -------
        str
            A string representation of the GeoBase object, including the name,
            dimension and tags.
        """
        return f"GeoBase {self.name}, dim={self.dim}, tags={self.tags}"

    def __add__(self, other):
        """
        Boolean union of two geometries.

        Parameters
        ----------
        other : GeoBase or list of GeoBase
            The geometry to perform the boolean operation with.

        Returns
        -------
        list of GeoBase
            The result of the boolean operation.

        Notes
        -----
        The boolean operation is performed by calling `occ.fuse`.

        See Also
        --------
        occ.fuse
        """
        return __boolean_operation__(self, other, occ.fuse)

    def __sub__(self, other):
        """
        Boolean difference of two geometries.

        Parameters
        ----------
        other : GeoBase or list of GeoBase
            The geometry to perform the boolean operation with.

        Returns
        -------
        list of GeoBase
            The result of the boolean operation.

        Notes
        -----
        The boolean operation is performed by calling `occ.cut`.

        See Also
        --------
        occ.cut
        """
        return __boolean_operation__(self, other, occ.cut)

    def __truediv__(self, other):
        """
        Boolean fragment of two geometries.

        Parameters
        ----------
        other : GeoBase or list of GeoBase
            The geometry to perform the boolean operation with.

        Returns
        -------
        list of GeoBase
            The result of the boolean operation.

        Notes
        -----
        The boolean operation is performed by calling `occ.fragment`.

        See Also
        --------
        occ.fragment
        """

        return __boolean_operation__(self, other, occ.fragment)

    def __mul__(self, other):
        """
        Boolean intersection of two geometries.

        Parameters
        ----------
        other : GeoBase or list of GeoBase
            The geometry to perform the boolean operation with.

        Returns
        -------
        list of GeoBase
            The result of the boolean operation.

        Notes
        -----
        The boolean operation is performed by calling `occ.intersect`.

        See Also
        --------
        occ.intersect
        """
        return __boolean_operation__(self, other, occ.intersect)

    def rotate(self, point, angle, axis=(0, 0, 1)):
        """Rotate the geometry around an axis passing through a given point.

        Parameters
        ----------
        point : tuple of float
            The coordinates of the point.
        angle : float
            The angle of rotation in radians.
        axis : tuple of float, optional
            The direction of the axis of rotation. Defaults to (0, 0, 1).

        Returns
        -------
        self
        """
        occ.rotate([self.dimtag], *point, *axis, angle)
        return self

    def __len__(self):
        return 0


class Geometry:
    """Base class for geometry models."""

    def __init__(
        self,
        dim=2,
        model_name=None,
        data_dir=None,
        gmsh_args=None,
        options=None,
        quad=False,
    ):
        self.dim = dim
        self.model_name = model_name or f"model_{self.__hash__()}"
        self.data_dir = data_dir or tempfile.mkdtemp()
        self.gmsh_args = gmsh_args or []
        self.options = options or {}
        self.quad = quad

        self.mesh_name = f"{self.model_name}.msh"
        self.subdomains = dict(volumes={}, surfaces={}, curves={}, points={})
        self.occ = occ
        self.model = model
        self.mesh = None

        _add_gmsh_methods(self)
        self._gmsh_add_disk = self.add_disk
        del self.add_disk
        self._gmsh_add_ellipse = self.add_ellipse
        del self.add_ellipse
        self._gmsh_add_spline = self.add_spline
        del self.add_spline
        self._gmsh_add_rectangle = self.add_rectangle
        del self.add_rectangle
        self._gmsh_add_point = self.add_point
        del self.add_point
        self._gmsh_add_line = self.add_line
        del self.add_line
        self._gmsh_add_box = self.add_box
        del self.add_box
        self._gmsh_add_sphere = self.add_sphere
        del self.add_sphere

        self.finalize()

        if not gmsh.is_initialized():
            gmsh.initialize(self.gmsh_args)

        for k, v in self.options.items():
            gmsh_options.set(k, v)

        if quad:
            set_quadratic_meshing()
        else:
            unset_quadratic_meshing()

    def is_initialized(self):
        """
        Check if Gmsh has been initialized.

        Returns
        -------
        bool
            Whether Gmsh has been initialized.

        """
        gmsh.is_initialized()

    def finalize(self):
        """
        Finalize Gmsh.

        This function is automatically called when the geometry object is
        garbage collected. It is generally not necessary to call it explicitly.

        Notes
        -----
        This function does not raise an exception if Gmsh is not initialized.
        """
        if gmsh.is_initialized():
            gmsh.finalize()
            # try:
            #     gmsh.finalize()
            # except Exception:
            #     pass

    def _check_dim(self, dim):
        """
        Check the dimension of a geometry entity.

        If `dim` is None, the dimension of the geometry is returned.
        Otherwise, `dim` is returned as is.

        Parameters
        ----------
        dim : int or None
            The dimension of the entity.

        Returns
        -------
        int
            The dimension of the entity.
        """
        return self.dim if dim is None else dim

    def rotate(self, tag, point, axis, angle, dim=None):
        """
        Rotate a geometry entity.

        Parameters
        ----------
        tag : int or list of int
            The tag(s) of the entity(ies) to rotate.
        point : tuple of float
            The coordinates of the rotation center.
        axis : tuple of float
            The direction of the rotation axis.
        angle : float
            The angle of rotation in radians.
        dim : int or None
            The dimension of the entity. If None, the dimension of the geometry
            is used.

        Returns
        -------
        self
        """
        dt = self.dimtag(tag, dim=dim)
        return occ.rotate(dt, *point, *axis, angle)

    def add_physical(self, idf, name, dim=None):
        """
        Add a physical group.

        Parameters
        ----------
        idf : int or list of int
            The tag(s) of the entity(ies) to add.
        name : str
            The name of the physical group.
        dim : int or None
            The dimension of the entity. If None, the dimension of the geometry
            is used.

        Returns
        -------
        int
            The tag of the physical group.
        """
        dim = self._check_dim(dim)
        dicname = list(self.subdomains)[3 - dim]
        if not isinstance(idf, list):
            idf = [idf]
        num = gmsh.model.addPhysicalGroup(dim, idf)
        self.subdomains[dicname][name] = num
        gmsh.model.removePhysicalName(name)
        gmsh.model.setPhysicalName(dim, self.subdomains[dicname][name], name)
        return num

    def add(self, entity, name=None):
        """
        Add a physical group to the geometry.

        Parameters
        ----------
        entity : GeoBase or list of GeoBase
            The entity(ies) to add to the physical group.
        name : str or None
            The name of the physical group. If None, the name of the entity is
            used.

        Returns
        -------
        int
            The tag of the physical group.

        Notes
        -----
        The physical group is added to the geometry with the same dimension as
        the entity(ies).

        """
        name = name or entity.name
        if len(entity) > 0:
            tag = [e.tag for e in entity]
            dim = [e.dim for e in entity]
            assert np.allclose(dim, dim[0])
            dim = dim[0]
        else:
            tag = entity.tag
            dim = entity.dim
        phys_tag = self.add_physical(tag, name, dim=dim)
        if len(entity) > 0:
            for e in entity:
                e.physical_tag = phys_tag
                e.name = name
        else:
            entity.physical_tag = phys_tag
            entity.name = name
        return phys_tag

    def dimtag(self, idf, dim=None):
        """
        Get the dimension and tag of an entity.

        Parameters
        ----------
        idf : int or list of int
            The tag(s) of the entity(ies).
        dim : int or None
            The dimension of the entity. If None, the dimension of the geometry
            is used.

        Returns
        -------
        tuple of int
            A tuple of two elements: the dimension and the tag of the entity.

        """
        dim = self._check_dim(dim)

        return _dimtag(idf, dim=dim)

    def tagdim(self, x):
        """
        Return the tag of the entity(ies).

        Parameters
        ----------
        x : int or list of int
            The tag(s) of the entity(ies).

        Returns
        -------
        list of int
            The tag(s) of the entity(ies).

        """
        if not isinstance(x, list):
            x = [x]
        return [t[1] for t in x]

    def _translation_matrix(self, t):
        """
        Return a 4x4 translation matrix.

        Parameters
        ----------
        t : tuple of 3 floats
            The translation vector.

        Returns
        -------
        list of 16 floats
            The translation matrix.

        """
        M = [
            1.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
        ]
        M[3], M[7], M[11] = t
        return M

    # def add_circle(self,x, y, z, ax, ay,**kwargs):
    #     ell = self._gmsh_add_ellipse(x, y, z, ax, ay,**kwargs)
    #     ell = self.add_curve_loop([ell])
    #     return self.add_plane_surface([ell])

    def add_point(self, coords, **kwargs):
        """
        Add a point.

        Parameters
        ----------
        coords : tuple of 3 floats
            The coordinates of the point.

        Returns
        -------
        int
            The tag of the point.
        """
        return self._gmsh_add_point(*coords, **kwargs)

    def add_line(self, p1, p2, **kwargs):
        """
        Add a line.

        Parameters
        ----------
        p1, p2 : tuple of 3 floats or GeoBase
            The coordinates of the points or the GeoBase objects.

        Returns
        -------
        int
            The tag of the line.

        Notes
        -----
        If p1 and p2 are not GeoBase objects, they are added as points and
        their tags are used to create the line.

        """
        ps = []
        for p in [p1, p2]:
            if isinstance(p, GeoBase):
                ps.append(p.tag)
            elif not isinstance(p, int):
                p = self.add_point(p, **kwargs)
                ps.append(p.tag)
            else:
                ps.append(p)
        return self._gmsh_add_line(*ps, **kwargs)

    def add_disk(self, center, radius, **kwargs):
        """
        Add a disk.

        Parameters
        ----------
        center : tuple of 3 floats
            The coordinates of the center of the disk.
        radius : float
            The radius of the disk.

        Returns
        -------
        int
            The tag of the disk.

        Notes
        -----
        The disk is created by calling ``add_ellipse`` with the same radius
        for both axes.

        """
        return self.add_ellipse(center, radius, radius, **kwargs)

    def add_ellipse(self, center, ax, ay, **kwargs):
        """
        Add an ellipse.

        Parameters
        ----------
        center : tuple of 3 floats
            The coordinates of the center of the ellipse.
        ax : float
            The length of the semi-axis aligned with the x-axis.
        ay : float
            The length of the semi-axis aligned with the y-axis.

        Returns
        -------
        int
            The tag of the ellipse.

        Notes
        -----
        The ellipse is created by calling ``add_disk`` with the same radius
        for both axes, and then rotating it by 90 degrees around the z-axis
        if the semi-axis aligned with the x-axis is shorter than the one
        aligned with the y-axis.

        """
        if ax < ay:
            ell = self._gmsh_add_disk(*center, ay, ax, **kwargs)
            self.rotate(ell.tag, center, (0, 0, 1), np.pi / 2, dim=2)
            return ell
        else:
            return self._gmsh_add_disk(*center, ax, ay, **kwargs)

    def add_rectangle(self, corner, dx, dy, **kwargs):
        """
        Add a rectangle.

        Parameters
        ----------
        corner : tuple of 3 floats
            The coordinates of the bottom left corner of the rectangle.
        dx : float
            The width of the rectangle.
        dy : float
            The height of the rectangle.

        Returns
        -------
        int
            The tag of the rectangle.
        """
        tag = self._gmsh_add_rectangle(*corner, dx, dy, **kwargs)
        return tag

    def add_square(self, corner, dx, **kwargs):
        """
        Add a square.

        Parameters
        ----------
        corner : tuple of 3 floats
            The coordinates of the bottom left corner of the square.
        dx : float
            The side length of the square.

        Returns
        -------
        int
            The tag of the square.
        """

        return self.add_rectangle(corner, dx, dx, **kwargs)

    def add_box(self, coords, widths, **kwargs):
        """
        Add a box.

        Parameters
        ----------
        coords : tuple of 3 floats
            The coordinates of the bottom left corner of the box.
        widths : tuple of 3 floats
            The width, height and depth of the box.

        Returns
        -------
        int
            The tag of the box.
        """
        return self._gmsh_add_box(*coords, *widths, **kwargs)

    def add_sphere(self, center, radius, **kwargs):
        """
        Add a sphere.

        Parameters
        ----------
        center : tuple of 3 floats
            The coordinates of the center of the sphere.
        radius : float
            The radius of the sphere.

        Returns
        -------
        int
            The tag of the sphere.
        """
        return self._gmsh_add_sphere(*center, radius, **kwargs)

    def add_polygon(self, vertices, mesh_size=0.0, **kwargs):
        """
        Add a polygon.

        Parameters
        ----------
        vertices : list of tuples of 3 floats
            The coordinates of the vertices of the polygon.
        mesh_size : float, optional
            The mesh size of the points. Default is 0.0.
        **kwargs : dict
            Additional keyword arguments are passed to the curve loop.

        Returns
        -------
        int
            The tag of the plane surface.

        Notes
        -----
        The polygon is created by adding the points and lines of the polygon,
        then creating a curve loop and plane surface from the lines.

        """
        verts = np.array(vertices)
        N = len(vertices)
        points = []
        for coord in verts:
            p0 = self.add_point(coord, meshSize=mesh_size)
            points.append(p0)
        lines = []
        for i in range(N - 1):
            lines.append(self.add_line(points[i], points[i + 1]))
        if not np.allclose(coord[0], coord[-1]):
            lines.append(self.add_line(points[i + 1], points[0]))
        lines = [line.tag for line in lines]
        loop = self.add_curve_loop(lines, **kwargs)
        loop = loop.tag
        return self.add_plane_surface([loop])

    def add_spline(self, points, mesh_size=0.0, **kwargs):
        """
        Add a spline.

        Parameters
        ----------
        points : list of tuples of 3 floats
            The coordinates of the points of the spline.
        mesh_size : float, optional
            The mesh size of the points. Default is 0.0.
        **kwargs : dict
            Additional keyword arguments are passed to the curve loop.

        Returns
        -------
        int
            The tag of the plane surface.

        Notes
        -----
        The spline is created by adding the points and lines of the spline,
        then creating a curve loop and plane surface from the lines.

        """
        dt = [self.add_point(p, meshSize=mesh_size) for p in points]
        if not np.allclose(points[0], points[-1]):
            dt.append(dt[0])
        dt = [d.tag for d in dt]
        spl = self._gmsh_add_spline(dt, **kwargs)
        spl = self.add_curve_loop([spl.tag])
        return self.add_plane_surface([spl.tag])

    def fragment(self, id1, id2, dim1=None, dim2=None, sync=True, map=False, **kwargs):
        """
        Fragment an entity into several entities.

        Parameters
        ----------
        id1, id2 : int
            The tags of the entities to fragment.
        dim1, dim2 : int, optional
            The dimension of the entities. If None, the dimension of the
            geometry is used.
        sync : bool, optional
            If True, call `occ.synchronize` after the fragmentation.
        map : bool, optional
            If True, return the mapping of the original entities to the
            resulting entities.
        **kwargs : dict
            Additional keyword arguments are passed to OCC.

        Returns
        -------
        list of int or tuple of list of int and dict
            If `map` is False, return the tags of the resulting entities.
            If `map` is True, return a tuple containing the tags of the
            resulting entities and a dictionary mapping the original
            entities to the resulting entities.

        Notes
        -----
        The OCC fragmentation algorithm is used.

        """
        dim1 = self._check_dim(dim1)
        dim2 = self._check_dim(dim2)
        a1 = self.dimtag(id1, dim1)
        a2 = self.dimtag(id2, dim2)
        dimtags, mapping = occ.fragment(a1, a2, **kwargs)
        if sync:
            occ.synchronize()
        tags = [_[1] for _ in dimtags]
        return (tags, mapping) if map else tags

    def intersect(self, id1, id2, dim1=None, dim2=None, sync=True, map=False, **kwargs):
        """
        Intersect two entities.

        Parameters
        ----------
        id1, id2 : int
            The tags of the entities to intersect.
        dim1, dim2 : int, optional
            The dimension of the entities. If None, the dimension of the
            geometry is used.
        sync : bool, optional
            If True, call `occ.synchronize` after the intersection.
        map : bool, optional
            If True, return the mapping of the original entities to the
            resulting entities.
        **kwargs : dict
            Additional keyword arguments are passed to OCC.

        Returns
        -------
        list of int or tuple of list of int and dict
            If `map` is False, return the tags of the resulting entities.
            If `map` is True, return a tuple containing the tags of the
            resulting entities and a dictionary mapping the original
            entities to the resulting entities.

        Notes
        -----
        The OCC intersection algorithm is used.

        """
        dim1 = self._check_dim(dim1)
        dim2 = self._check_dim(dim2)
        a1 = self.dimtag(id1, dim1)
        a2 = self.dimtag(id2, dim2)
        dimtags, mapping = occ.intersect(a1, a2, **kwargs)
        if sync:
            occ.synchronize()
        tags = [_[1] for _ in dimtags]
        return (tags, mapping) if map else tags

    def cut(self, id1, id2, dim1=None, dim2=None, sync=True, **kwargs):
        """
        Cut an entity with another entity.

        Parameters
        ----------
        id1, id2 : int
            The tags of the entities to cut.
        dim1, dim2 : int, optional
            The dimension of the entities. If None, the dimension of the
            geometry is used.
        sync : bool, optional
            If True, call `occ.synchronize` after the cut.
        **kwargs : dict
            Additional keyword arguments are passed to OCC.

        Returns
        -------
        list of int
            The tags of the resulting entities.

        Notes
        -----
        The OCC cut algorithm is used.

        """
        dim1 = self._check_dim(dim1)
        dim2 = self._check_dim(dim2)
        a1 = self.dimtag(id1, dim1)
        a2 = self.dimtag(id2, dim2)
        ov, ovv = occ.cut(a1, a2, **kwargs)
        if sync:
            occ.synchronize()
        return [o[1] for o in ov]

    def fuse(self, id1, id2, dim1=None, dim2=None, sync=True):
        """
        Fuse two entities.

        Parameters
        ----------
        id1, id2 : int
            The tags of the entities to fuse.
        dim1, dim2 : int, optional
            The dimension of the entities. If None, the dimension of the
            geometry is used.
        sync : bool, optional
            If True, call `occ.synchronize` after the fusion.

        Returns
        -------
        list of int
            The tags of the resulting entities.

        Notes
        -----
        The OCC fusion algorithm is used.

        """
        dim1 = self._check_dim(dim1)
        dim2 = self._check_dim(dim2)
        a1 = self.dimtag(id1, dim1)
        a2 = self.dimtag(id2, dim2)
        ov, ovv = occ.fuse(a1, a2)
        if sync:
            occ.synchronize()
        return [o[1] for o in ov]

    def get_boundaries(self, idf, dim=None, physical=True):
        """
        Get the boundary entities of a given entity.

        Parameters
        ----------
        idf : int or str
            Label or list of labels.
        dim : int
            Dimension.
        physical : bool, optional
            If True, get the physical group, else get the tag.

        Returns
        -------
        list of int
            A list of entity tags of the boundary entities.

        """
        dim = self._check_dim(dim)
        if isinstance(idf, str):
            if dim == 2:
                type_entity = "surfaces"
            elif dim == 3:
                type_entity = "volumes"
            else:
                type_entity = "curves"
            idf = self.subdomains[type_entity][idf]

            n = gmsh.model.getEntitiesForPhysicalGroup(dim, idf)
            bnds = [_get_bnd(n_, dim=dim) for n_ in n]
            bnds = [item for sublist in bnds for item in sublist]
            return list(dict.fromkeys(bnds))
        else:
            n = gmsh.model.getEntitiesForPhysicalGroup(dim, idf)[0] if physical else idf
            return _get_bnd(n, dim=dim)

    def _set_size(self, idf, s, dim=None):
        """
        Set the mesh size of an entity.

        Parameters
        ----------
        idf : int
            The tag of the entity.
        s : float
            The mesh size.
        dim : int, optional
            The dimension of the entity. If None, the dimension of the
            geometry is used.

        Returns
        -------
        None

        """
        dim = self._check_dim(dim)
        p = gmsh.model.getBoundary(
            self.dimtag(idf, dim=dim), False, False, True
        )  # Get all points
        gmsh.model.mesh.setSize(p, s)

    def _check_subdomains(self):
        """
        Check the subdomains of the geometry and remove those that are not present
        anymore in the Gmsh model.

        This is useful when the geometry has been modified and some subdomains
        have been deleted.

        Parameters
        ----------
        None

        Returns
        -------
        None

        """
        groups = gmsh.model.getPhysicalGroups()
        names = [gmsh.model.getPhysicalName(*g) for g in groups]
        for subtype, subitems in self.subdomains.items():
            for idf in subitems.copy().keys():
                if idf not in names:
                    subitems.pop(idf)

    def set_mesh_size(self, params, dim=None):
        """
        Set the mesh size of entities.

        Parameters
        ----------
        params : dict
            A dictionary with the tags or names of the entities as keys and the
            mesh size as values.
        dim : int, optional
            The dimension of the entities. If None, the dimension of the
            geometry is used.

        Returns
        -------
        None

        Notes
        -----
        The mesh size is set in the order of increasing mesh size, so that
        smaller mesh sizes are set last.

        """
        dim = self._check_dim(dim)
        if dim == 3:
            type_entity = "volumes"
        elif dim == 2:
            type_entity = "surfaces"
        elif dim == 1:
            type_entity = "curves"
        elif dim == 0:
            type_entity = "points"

        # revert sort so that smaller sizes are set last
        params = dict(
            sorted(params.items(), key=lambda item: float(item[1]), reverse=True)
        )

        for idf, p in params.items():
            if isinstance(idf, str):
                num = self.subdomains[type_entity][idf]
                n = gmsh.model.getEntitiesForPhysicalGroup(dim, num)
                for n_ in n:
                    self._set_size(n_, p, dim=dim)
            else:
                self._set_size(idf, p, dim=dim)

    def set_size(self, idf, s, dim=None):
        """
        Set the mesh size of an entity or a list of entities.

        Parameters
        ----------
        idf : int or list of int
            The tag of the entity or the list of tags of the entities.
        s : float or list of float
            The mesh size or the list of mesh sizes.
        dim : int, optional
            The dimension of the entity. If None, the dimension of the
            geometry is used.

        Returns
        -------
        None

        Notes
        -----
        The mesh size is set in the order of increasing mesh size, so that
        smaller mesh sizes are set last.

        """
        if hasattr(idf, "__len__") and not isinstance(idf, str):
            for i, id_ in enumerate(idf):
                s_ = s[i] if hasattr(s, "__len__") else s
                params = {id_: s_}
                self.set_mesh_size(params, dim=dim)
        else:
            self.set_mesh_size({idf: s}, dim=dim)

    def read_mesh_info(self):
        """
        Read the mesh information.

        Notes
        -----
        The mesh information is read from the mesh file and stored in the
        following attributes:

        - `domains`: the subdomains of the geometry.
        - `lines`: the lines of the geometry (2D only).
        - `boundaries`: the boundaries of the geometry (2D and 3D).
        - `points`: the points of the geometry.

        """
        if self.dim == 1:
            self.domains = self.subdomains["curves"]
            self.lines = {}
            self.boundaries = {}
        elif self.dim == 2:
            self.domains = self.subdomains["surfaces"]
            self.lines = {}
            self.boundaries = self.subdomains["curves"]
        else:
            self.domains = self.subdomains["volumes"]
            self.lines = self.subdomains["curves"]
            self.boundaries = self.subdomains["surfaces"]

        self.points = self.subdomains["points"]

    @property
    def msh_file(self):
        """
        The path to the .msh file.

        Returns
        -------
        str
            The path to the .msh file.
        """
        return os.path.join(self.data_dir, self.mesh_name)

    def build(
        self,
        interactive=False,
        generate_mesh=True,
        write_mesh=True,
        read_info=True,
        read_mesh=True,
        check_subdomains=True,
    ):
        """
        Build the mesh.

        Parameters
        ----------
        interactive : bool, optional
            If True, start the Gmsh GUI. Default is False.
        generate_mesh : bool, optional
            If True, generate the mesh. Default is True.
        write_mesh : bool, optional
            If True, write the mesh to file. Default is True.
        read_info : bool, optional
            If True, read the mesh information (points, curves, surfaces, volumes).
            Default is True.
        read_mesh : bool, optional
            If True, read the mesh. Default is True.
        check_subdomains : bool, optional
            If True, check if the subdomains are still present in the Gmsh model.
            If a subdomain is not present, it is removed from the dictionary.
            Default is True.

        Returns
        -------
        mesh : meshio.Mesh
            The mesh.

        """
        if check_subdomains:
            self._check_subdomains()

        self.mesh = self.generate_mesh(
            generate=generate_mesh, write=write_mesh, read=read_mesh
        )

        if read_info:
            self.read_mesh_info()
        if interactive:  # pragma: no cover
            gmsh.fltk.run()  # pragma: no cover
        return self.mesh

    def read_mesh_file(self, subdomains=None):
        """
        Read a mesh file and return the mesh.

        Parameters
        ----------
        subdomains : list of str, optional
            The subdomains to be read. If None, all subdomains are read.

        Returns
        -------
        mesh : meshio.Mesh
            The read mesh.

        Notes
        -----
        The mesh is read using meshio.read.

        See Also
        --------
        meshio.read

        """
        if subdomains is not None:
            if isinstance(subdomains, str):
                subdomains = [subdomains]
            key = "volumes" if self.dim == 3 else "surfaces"
            subdomains_num = [self.subdomains[key][s] for s in subdomains]
        else:
            subdomains_num = subdomains

        return read_mesh(
            self.msh_file,
            data_dir=self.data_dir,
            dim=self.dim,
            subdomains=subdomains_num,
            quad=self.quad,
        )

    def generate_mesh(self, generate=True, write=True, read=True):
        """
        Generate the mesh, write it to file and read it.

        Parameters
        ----------
        generate : bool, optional
            If True, generate the mesh. Default is True.
        write : bool, optional
            If True, write the mesh to file. Default is True.
        read : bool, optional
            If True, read the mesh from file. Default is True.

        Returns
        -------
        mesh : meshio.Mesh
            The generated mesh if `read` is True, else None.
        """
        if generate:
            gmsh.model.mesh.generate(self.dim)
        if write:
            gmsh.write(self.msh_file)
        if read:
            return self.read_mesh_file()


# def is_on_plane(P, A, B, C, eps=1e-12):
#     Ax, Ay, Az = A
#     Bx, By, Bz = B
#     Cx, Cy, Cz = C

#     a = (By - Ay) * (Cz - Az) - (Cy - Ay) * (Bz - Az)
#     b = (Bz - Az) * (Cx - Ax) - (Cz - Az) * (Bx - Ax)
#     c = (Bx - Ax) * (Cy - Ay) - (Cx - Ax) * (By - Ay)
#     d = -(a * Ax + b * Ay + c * Az)

#     return np.allclose(a * P[0] + b * P[1] + c * P[2] + d, 0, atol=eps)


# def is_on_line(p, p1, p2, eps=1e-12):
#     x, y = p
#     x1, y1 = p1
#     x2, y2 = p2
#     return np.allclose((y - y1) * (x2 - x1), (y2 - y1) * (x - x1), atol=eps)


# def is_on_line3D(p, p1, p2, eps=1e-12):
#     return is_on_plane(p, *p1, eps=eps) and is_on_plane(p, *p2, eps=eps)


def plot_geometry(ax, geom, **kwargs):
    """
    Plot the geometry.

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The axis on which to plot the geometry.
    geom : geolia.geometry.Geometry
        The geometry to be plotted.

    Returns
    -------
    triplt : matplotlib.tri.TriMesh
        The triangulation plot of the geometry.
    cax : matplotlib.colorbar.Colorbar
        The colorbar of the triangulation plot.

    Notes
    -----
    The colorbar is labeled with the names of the physical groups
    present in the geometry.

    """
    mesh = geom.mesh["triangle"]
    data = mesh.cell_data["triangle"][0]
    cmap = kwargs.pop("cmap", "tab20")
    cmap = plt.get_cmap(cmap, np.max(data) - np.min(data) + 1)
    triplt = ax.tripcolor(
        mesh_to_triangulation(mesh),
        facecolors=data,
        vmin=np.min(data) - 0.5,
        vmax=np.max(data) + 0.5,
        cmap=cmap,
        **kwargs,
    )
    ax.set_aspect(1)
    ticks = [s for s in geom.subdomains["surfaces"]]
    cax = plt.colorbar(triplt, ticks=np.arange(np.min(data), np.max(data) + 1))
    cax.set_ticklabels(ticks)
    return triplt, cax


def set_quadratic_meshing():
    """
    Set the meshing algorithm to use quadratic elements.

    The following options are set:

    - `Mesh.RecombinationAlgorithm`: 2 (Simple algorithm)
    - `Mesh.RecombineAll`: 1 (recombine all elements)

    These options are used to control the meshing algorithm. See Gmsh's
    documentation for more information.

    """
    gmsh.option.setNumber("Mesh.RecombinationAlgorithm", 2)
    gmsh.option.setNumber("Mesh.RecombineAll", 1)


def unset_quadratic_meshing():
    """
    Unset the quadratic meshing algorithm.

    The following options are set:

    - `Mesh.RecombineAll`: 0 (do not recombine all elements)

    These options are used to control the meshing algorithm. See Gmsh's
    documentation for more information.
    """
    gmsh.option.setNumber("Mesh.RecombineAll", 0)
