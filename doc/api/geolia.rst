geolia package
==============

Submodules
----------

geolia.geometry module
----------------------

.. automodule:: geolia.geometry
   :members:
   :undoc-members:
   :show-inheritance:

geolia.interp module
--------------------

.. automodule:: geolia.interp
   :members:
   :undoc-members:
   :show-inheritance:

geolia.mesh module
------------------

.. automodule:: geolia.mesh
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: geolia
   :members:
   :undoc-members:
   :show-inheritance:
