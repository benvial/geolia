#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of geolia
# License: GPLv3
# See the documentation at  gitlab.com/benvial/geolia


"""
3D geometry
============

Basic geometry operations.
"""


import matplotlib.pyplot as plt
import numpy as np

import geolia as gl

a = 1

######################################################################
# Create the 3D geometry object:

geom = gl.Geometry(dim=3, quad=False)


######################################################################
# Add a square:

corner = -a / 2, -a / 2, -a / 2
center = [0, 0, 0]
cell = geom.add_box(corner, (a, a, a))

######################################################################
# Add a sphere:
r = a / 3
incl = geom.add_sphere(center, r)

######################################################################
# Fragment boolean operation ``/``:

incl, cell = cell / incl


######################################################################
# Add the geometric objects as physical groups:

geom.add(cell, "cell")
geom.add(incl, "inclusion")

######################################################################
# Set the mesh size:

geom.set_size("cell", a / 10)
geom.set_size("inclusion", a / 20)

######################################################################
# Now build the model and finalize the gmsh API:

geom.build()
geom.finalize()
Npts = 21
x = y = z = np.linspace(-a / 2, a / 2, Npts)
x1, y1, z1 = np.meshgrid(x, y, z)


######################################################################
# Interpolate:

domains = gl.interpolate((x1, y1, z1), geom.mesh, cell="tetra")

i = 11

plt.figure()
plt.pcolormesh(x, y, domains[:, :, i])
plt.axis("scaled")
plt.title(f"z = {z[i]}")
plt.show()
