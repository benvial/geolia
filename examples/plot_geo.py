#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of geolia
# License: GPLv3
# See the documentation at  gitlab.com/benvial/geolia


"""
Showcase
=========

Basic geometry operations.
"""

from math import pi

import matplotlib.pyplot as plt
import numpy as np

import geolia as gl

a = 1

######################################################################
# Create the 2D geometry object:

geom = gl.Geometry(dim=2)

######################################################################
# Add a point:

vertices = (0, a, 0), (a, a, 0), (a, 2 * a, 0)
p = geom.add_point(vertices[0])

######################################################################
# Add a line:

line = geom.add_line(vertices[0], vertices[1])


######################################################################
# Add a square:

corner = -a / 2, -a / 2, 0
r = 0.3
center = [0, 0, 0]
cell = geom.add_square(corner, a)
print(cell)

######################################################################
# Rotate it:

cell.rotate(center, pi / 3)

######################################################################
# Add a disk:

circ = geom.add_disk(center, r)

######################################################################
# Fuse boolean operation ``+``:

circ += geom.add_disk((0, -0.3, 0), r / 2)

######################################################################
# Fragment boolean operation ``/``:

circ, cell = cell / circ
print(cell)

######################################################################
# Add disks:
circ1 = geom.add_disk((0.1, 0, 0), r / 4)
circ2 = geom.add_disk((0, 0.2, 0), r / 2)

######################################################################
# Intersection boolean operation ``*``:

circ2 *= geom.add_square((0.1, 0.1, 0), r / 2)

######################################################################
# Cut boolean operation ``-``:

circ -= [circ1, circ2]

######################################################################
# Add more disks and compute fragments:

test1 = geom.add_disk((0.1, 0.4, 0), 0.1 * r)
test2 = geom.add_disk((0.3, 0.3, 0), 0.2 * r)
test = [test1, test2]
*test, cell = cell / test

######################################################################
# Add polygon and spline:

# add a polygon from the same vertices as the square
pol = geom.add_polygon(vertices)
# add a spline from the same vertices as the square, shifted
vertices_spl = np.array(vertices) + np.array([1, -2, 0])
spl = geom.add_spline(vertices_spl)

######################################################################
# Add the geometric objects as physical groups:

geom.add(line, "line")
geom.add(pol, "triangle")
geom.add(spl, "spline")
geom.add(cell, "cell")
geom.add(circ)
geom.add(test, "whatever")
print(cell)

######################################################################
# Set the mesh size:

# set a different mesh size for each physical group
geom.set_size("cell", 0.08)
geom.set_size(circ.name, 0.01)
geom.set_size("whatever", 0.01)
geom.set_size("triangle", 0.1)
geom.set_size("spline", 0.1)

######################################################################
# Now build the model and finalize the gmsh API:

geom.build()
geom.finalize()


######################################################################
# Plot the mesh:

# create a triangulation object from the mesh
mesh = geom.mesh["triangle"]
# create a figure and axis object
_, ax = plt.subplots()
# plot the mesh
gl.plot_mesh(ax, mesh, lw=0.4, color="#379798")
# add labels and title
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title("mesh")
# show the plot
plt.show()

######################################################################
# Plot the geometry:
_, ax = plt.subplots()
ax, cax = gl.plot_geometry(ax, geom)
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title("subdomains")
plt.show()
