#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of geolia
# License: GPLv3
# See the documentation at  gitlab.com/benvial/geolia


"""
Interpolation
=============

Unstructured to structured grid. This example shows how to define a
geometry, generate a mesh, refine the mesh on boundaries, and
interpolate data from the unstructured mesh to a structured grid.

"""

import matplotlib.pyplot as plt
import numpy as np

import geolia as gl

######################################################################
# Define the geometry.

a = 1
pmesh = a / 15
corner = -a / 2, -a / 2, 0
r = 0.3
center = [0, 0, 0]

# Create the geometry.
geom = gl.Geometry(
    2,
    options={
        "Mesh.MeshSizeExtendFromBoundary": False,
        "Mesh.MeshSizeFromPoints": False,
        "Mesh.MeshSizeFromCurvature": False,
    },
)

# Add the square and the circular inclusion.
cell = geom.add_square(corner, a)
circ = geom.add_disk(center, r)
circ, cell = cell / circ
circ_cut = geom.add_disk((0, 0.8 * r, 0), r * 0.7)
# Cut circ_cut from circ and cell.
incl, *cell = circ / [circ_cut, cell]

# Add the subdomains to the geometry.
geom.add(cell, "cell")
geom.add(incl, "inclusion")
circ_bnds = geom.get_boundaries("inclusion")


######################################################################
# Refine the mesh on the boundaries of the inclusion.

# Create a field to refine the mesh.
field = geom.model.mesh.field.add("Distance")
geom.model.mesh.field.set_numbers(field, "CurvesList", circ_bnds)
geom.model.mesh.field.set_number(field, "Sampling", 100)

# Create a threshold to set the mesh size.
thres = geom.model.mesh.field.add("Threshold")
geom.model.mesh.field.set_number(thres, "InField", field)
geom.model.mesh.field.set_number(thres, "SizeMin", pmesh / 10)
geom.model.mesh.field.set_number(thres, "SizeMax", pmesh)
geom.model.mesh.field.set_number(thres, "DistMin", 0.01)
geom.model.mesh.field.set_number(thres, "DistMax", 0.3)

# Set the threshold as the background mesh.
geom.model.mesh.field.set_as_background_mesh(thres)


######################################################################
# Build the mesh and finalize the geometry.

geom.build()
geom.finalize()


######################################################################
# Plot the geometry.

_, ax = plt.subplots()
ax, cax = gl.plot_geometry(ax, geom, cmap="inferno")
gl.plot_mesh(plt.gca(), geom.mesh["triangle"], lw=0.4, color="#379798")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.tight_layout()


######################################################################
# Interpolate data from the unstructured mesh to a structured grid.

# Define the structured grid.
n = 2**8
x = y = np.linspace(-a / 2, a / 2, n)
X, Y = np.meshgrid(x, y)

# Interpolate data from the unstructured mesh to the structured grid.
Z = gl.interpolate([X, Y], geom.mesh, "triangle", "nearest")
plt.figure()
plt.pcolormesh(X, Y, Z, shading="auto", cmap="inferno")
plt.axis("scaled")
plt.colorbar()
plt.tight_layout()
